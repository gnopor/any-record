// https://web.dev/offline-cookbook/

const STATIC_CACHE_NAME = "site-static-v1";
const DYNAMIC_CACHE_NAME = "site-dynamic-v1";
const DYNAMIC_CACHE_SIZE_LIMIT = 500;
const OFFLINE_PAGE = "/offline/";
const STATIC_ASSETS = [
    OFFLINE_PAGE,
    "/home/",
    "/favicon.ico",
    "/manifest.json",
    "/images/icons/homeScreen48x48.png",
    "/images/icons/homeScreen72x72.png",
    "/images/icons/homeScreen96x96.png",
    "/images/icons/homeScreen144x144.png",
    "/images/icons/homeScreen168x168.png",
    "/images/icons/homeScreen384x384.png",
    "/images/icons/homeScreen512x512.png"
];

// client-worker communication ----
const BROADCAST_CHANNEL_NAME = "channel-any-record-1";
const SKIP_WAITING_MESSAGE = "SKIP_WAITING";
const CONNECTION_STATUS_MESSAGE = "CONNECTION_STATUS";
const RELOAD_APP_MESSAGE = "RELOAD_APP";

const broadcast = new BroadcastChannel(BROADCAST_CHANNEL_NAME);

let isClientConnected = true;
let offlineRequests = [];

// installed SW event
self.addEventListener("install", (event) => {
    event.waitUntil(loadStaticAssets(STATIC_CACHE_NAME, STATIC_ASSETS));
});

// activated SW event
self.addEventListener("activate", (event) => {
    event.waitUntil(deleteOutdatedAssests([STATIC_CACHE_NAME, DYNAMIC_CACHE_NAME]));
});

// message SW event
broadcast.addEventListener("message", (event) => {
    if (!event?.data?.type) return;

    if (event.data.type === SKIP_WAITING_MESSAGE) {
        return self.skipWaiting();
    }

    if (event.data.type === CONNECTION_STATUS_MESSAGE) {
        isClientConnected = event.data.data.status;
        if (isClientConnected) sendSavedRequests();

        console.log(`[SW CONNECTION STATUS] ${isClientConnected ? "Online" : "Offline"}`);
        return;
    }
});

// fetch SW event (When respondWith is not called, the browser handles the event)
self.addEventListener("fetch", (event) => {
    const request = event.request;
    const acceptHeader = request.headers.get("Accept") || "";
    const origin = self.origin;

    // kwown APIs Caching Strategy
    const toKnownAPIs = API_ENDPOINTS.find((endpoint) => request.url.startsWith(endpoint));
    if (request.method === "GET" && toKnownAPIs) {
        return event.respondWith(networkFirst(request));
    }
    if (request.method !== "GET" && toKnownAPIs) {
        const requestHandler = isClientConnected ? networkOnly : saveRequest;
        return event.respondWith(requestHandler(request));
    }

    // HTML Caching Strategy - Network First the Cache
    if (acceptHeader.includes("text/html") && request.url.startsWith(origin)) {
        return event.respondWith(networkFirst(request).catch(() => caches.match(OFFLINE_PAGE)));
    }

    // non HTML Caching Strategy - Cache First then Network (for images, css, js, videos, ...)
    if (!acceptHeader.includes("text/plain") && request.url.startsWith(origin)) {
        return event.respondWith(cacheFirst(request));
    }

    // Other Items Caching Strategy
    if (request.method === "GET") {
        return event.respondWith(staleWhileRevalidate(request));
    }
});

// Helpers -------------------------------
async function limitCacheSize(name, size = Infinity) {
    const cacheStorage = await caches.open(name);
    const keys = await cacheStorage.keys();

    if (keys.length > size) {
        await cacheStorage.delete(keys[0]);
        limitCacheSize(name, size);
    }
}

async function loadStaticAssets(cacheName, assets = []) {
    const cacheStorage = await caches.open(cacheName);
    await cacheStorage.addAll(assets);
}

async function deleteOutdatedAssests(validCaches = []) {
    const keys = await caches.keys();

    return Promise.all(
        keys.filter((key) => !validCaches.includes(key)).map((key) => caches.delete(key))
    );
}

// caching strategies ---------
async function networkOnly(request) {
    return fetch(request);
}

function cacheOnly(request) {
    return caches.match(request);
}

async function networkFirst(request) {
    try {
        const response = await fetch(request);
        const copy = response.clone();

        const cacheStorage = await caches.open(DYNAMIC_CACHE_NAME);
        cacheStorage.put(request, copy);
        limitCacheSize(DYNAMIC_CACHE_NAME, DYNAMIC_CACHE_SIZE_LIMIT);

        return response;
    } catch {
        return caches.match(request);
    }
}

async function cacheFirst(request) {
    const cacheResponse = await caches.match(request);
    if (cacheResponse) {
        return cacheResponse;
    }

    const response = await fetch(request);
    const copy = response.clone();

    const cacheStorage = await caches.open(DYNAMIC_CACHE_NAME);
    cacheStorage.put(request, copy);
    limitCacheSize(DYNAMIC_CACHE_NAME, DYNAMIC_CACHE_SIZE_LIMIT);

    return response;
}

async function staleWhileRevalidate(request) {
    try {
        const cacheStorage = await caches.open(DYNAMIC_CACHE_NAME);
        const cacheResponse = await cacheStorage.match(request);

        const fetchResponse = fetch(request).then((networkResponse) => {
            cacheStorage.put(request, networkResponse.clone());
            return networkResponse;
        });

        return cacheResponse || fetchResponse;
    } catch (error) {
        console.warn(error);
    }
}

// request syncronization
async function saveRequest(request) {
    console.log("[SW SAVE REQUEST]");

    offlineRequests.push(request);

    const responseBody = {
        message:
            "There is a network error. Your request has been registered and will be processed as soon as the internet connection is restored."
    };
    const responseOptions = { status: 400 };
    const response = new Response(JSON.stringify(responseBody), responseOptions);

    return response;
}

async function sendSavedRequests() {
    console.log("[SW SEND REQUESTS]");

    if (!offlineRequests.length) return;
    await Promise.allSettled(offlineRequests.map((r) => fetch(r)));
    offlineRequests = [];

    const reloadMessage = {
        type: RELOAD_APP_MESSAGE,
        data: {}
    };
    broadcast.postMessage(reloadMessage);
}
