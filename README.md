# PWA-OG

## About

![Hero section](./public/images/screenshot/hero_section.png)

```
ANY Record is an offline screen recorder that you can use to record your desktopp screen, any window on your screen or any tab of your browser. You can also download a ready-to-use video (your recording) to share with your team or save on your computer.

P.S.: You do not need an internet connection to use this tool.
P.S.S.: Don't thank me! It's FREE
```

[Try it here.](https://any-record.web.app)

## Screenshots

> Initial Preview

![Initial Preview](./public/images/screenshot/initial_recorder_preview.png)

> Recording Preview

![Recording Preview](./public/images/screenshot/recording_recorder_preview.png)

## Build Setup

```
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production (Generate Static Project)
$ npm run build

# deploy
$ npm run deploy

# deploy using CI pipeline
$ npm run deploy-ci
```

## Resources

-   Iconify react logo npm package
