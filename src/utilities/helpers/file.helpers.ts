export default class FileHelpers {
    static downloadVideo(url: string, options: { fileName?: string; onSucceed?: Function }) {
        const { fileName = "", onSucceed } = options;

        const link = document.createElement("a");
        link.setAttribute("href", url);
        link.setAttribute("download", fileName);

        link.click();
        link.remove();

        onSucceed?.();
    }
}
