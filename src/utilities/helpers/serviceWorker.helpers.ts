// source and explanation https://whatwebcando.today/articles/handling-service-worker-updates/

import Helpers from "./helpers";

interface IServiceWorkerMessageMap {
    SKIP_WAITING: undefined;
    CONNECTION_STATUS: { status: boolean };
}
type IServiceWorkerMessageType = keyof IServiceWorkerMessageMap;

const BROADCAST_CHANNEL_NAME = "channel-gudworker-1";
const RELOAD_APP_MESSAGE = "RELOAD_APP";

export default class ServiceWorkerHelpers {
    static async registerSW(serviceWorkerUrl: string) {
        if (!(window.navigator?.serviceWorker && window.BroadcastChannel)) return;

        const registration = await navigator.serviceWorker.register(serviceWorkerUrl);
        const broadcast = new BroadcastChannel(BROADCAST_CHANNEL_NAME);

        if (registration.waiting) {
            this.#sendMessageToServiceWorker(broadcast, "SKIP_WAITING", undefined);
        }

        registration.addEventListener("updatefound", () => {
            if (!registration.installing) return;

            registration.installing.addEventListener("statechange", () => {
                if (!registration.waiting) return;
                if (!navigator.serviceWorker.controller) return; // SW first initialiazation

                this.#sendMessageToServiceWorker(broadcast, "SKIP_WAITING", undefined);
            });
        });

        navigator.serviceWorker.addEventListener("controllerchange", () => {
            const result = window.confirm("New version of the app is available. Refresh now?");
            if (!result) return;

            window.location.reload();
        });

        Helpers.onConnectionStateChange((isConnected) =>
            this.#sendMessageToServiceWorker(broadcast, "CONNECTION_STATUS", {
                status: isConnected
            })
        );

        this.#runServiceWorkerToClientEventListener(broadcast);
    }

    static #sendMessageToServiceWorker<T extends IServiceWorkerMessageType>(
        broadcast: BroadcastChannel,
        type: T,
        data: IServiceWorkerMessageMap[T]
    ) {
        const message = { type, data };

        broadcast.postMessage(message);
    }

    static #runServiceWorkerToClientEventListener(broadcast: BroadcastChannel) {
        broadcast.addEventListener("message", (event) => {
            if (event?.data?.type === RELOAD_APP_MESSAGE) {
                window.location.reload();
                return;
            }
        });
    }
}
