/**
 *  You should regularly refactor this file and move functions
 *  that have outgrown the file into their own proper modules(services),
 *  otherwise there's a tendency that you'll just leave the functions
 *  there growing related functions without ever graduating into a proper module.
 */

export default class Helpers {
    static onConnectionStateChange(callback: (connected: boolean) => void) {
        callback(window.navigator.onLine);

        window.addEventListener("online", () => {
            callback(true);
        });
        window.addEventListener("offline", () => {
            callback(false);
        });
    }

    static formatDate(timestamp: number = 0, { withTime = false } = {}) {
        if (!timestamp) return "";

        const date = new Date(timestamp);
        const year = date.getFullYear();
        const month = this.formatDateUnit(1 + date.getMonth());
        const day = this.formatDateUnit(date.getDate());

        const hour = this.formatDateUnit(date.getHours());
        const minute = this.formatDateUnit(date.getMinutes());
        const second = this.formatDateUnit(date.getSeconds());

        const dateSection = `${day}/${month}/${year}`;
        const timeSection = withTime ? `${hour}:${minute}:${second}` : "";

        return `${dateSection} ${timeSection}`.trim();
    }

    static parseDateTime(timestamp: number = 0) {
        if (!timestamp) return "";

        const date = new Date(timestamp);
        const year = date.getFullYear();
        const month = this.formatDateUnit(1 + date.getMonth());
        const day = this.formatDateUnit(date.getDate());

        const hour = this.formatDateUnit(date.getHours());
        const minute = this.formatDateUnit(date.getMinutes());
        const second = this.formatDateUnit(date.getSeconds());

        const dateSection = `${year}-${month}-${day}`;
        const timeSection = `${hour}h${minute}m${second}s`;

        return `${dateSection}_${timeSection}`.trim();
    }

    static formatDateUnit(unit: number | string) {
        return unit.toString().padStart(2, "0");
    }
}
