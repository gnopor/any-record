import React from "react";
import Head from "next/head";

import DefaultLayout from "../layouts/DefaultLayout";
import { APP_NAME } from "../constants";

interface IProps {
    children: React.ReactNode;
    layout?: keyof typeof LAYOUTS_MAP;
    title?: string;
    private?: boolean;
    anonymOnly?: boolean;
}

const LAYOUTS_MAP = {
    DEFAULT_LAYOUT: DefaultLayout
};

export default function Page({ children, layout, title, private: isPrivate, anonymOnly }: IProps) {
    // const { isUserAuthenticated } = useAuth();

    // const router = useRouter();

    // const [isBrowser, setIsBrowser] = useState(false);

    const CurrentLayout = LAYOUTS_MAP[layout || "DEFAULT_LAYOUT"];

    // useEffect(() => {
    //     setIsBrowser(true);
    // }, []);

    // if ((isPrivate || anonymOnly) && !isBrowser) return <></>;

    // if (isPrivate && !isUserAuthenticated()) {
    //     router.push(PathHelpers.loginPagePath());
    //     return <></>;
    // }

    // if (anonymOnly && isUserAuthenticated()) {
    //     router.push(PathHelpers.homePagePath());
    //     return <></>;
    // }

    return (
        <>
            {title?.trim() && (
                <Head>
                    <title>
                        {title.trim()} | {APP_NAME}
                    </title>
                </Head>
            )}

            <CurrentLayout>{children}</CurrentLayout>
        </>
    );
}
