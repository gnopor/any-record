import { Icon as IConify } from "@iconify/react";

interface IProps {
    icon: "ep:close" | "bx:menu";
    width?: string;
}

export default function Icon({ icon, width }: IProps) {
    return <IConify icon={icon} width={width} />;
}
