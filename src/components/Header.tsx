import React, { useState } from "react";
import css from "styled-jsx/css";

import { APP_REPOSITORY } from "../constants";
import Icon from "./Icon";
import Link from "./Link";
import Logo from "./Logo";

const links = [
    { label: "contact", path: "https://tayoublaise.net/", target: "_blank" },
    { label: "source code", path: APP_REPOSITORY, target: "_blank" }
];

export default function Header() {
    const [isMenuVisible, setIsMenuVisible] = useState(false);

    const toggleMenuVisibility = () => {
        setIsMenuVisible((v) => !v);
    };

    return (
        <>
            <header>
                <nav className="container">
                    <figure className="logo">
                        <Link href="/home/">
                            <Logo />
                        </Link>
                    </figure>

                    <div className="trigger">
                        <button onClick={toggleMenuVisibility}>
                            {isMenuVisible ? (
                                <Icon icon="ep:close" width="30" />
                            ) : (
                                <Icon icon="bx:menu" width="30" />
                            )}
                        </button>
                    </div>

                    <ul className={`menu ${!isMenuVisible && "menu--collapse"}`}>
                        {links.map((l, i) => (
                            <li key={i} className="menu_item">
                                <Link href={l.path} {...(l.target ? { target: l.target } : {})}>
                                    <span>{l.label}</span>
                                </Link>
                            </li>
                        ))}
                    </ul>
                </nav>
            </header>

            <style jsx>{style}</style>
        </>
    );
}

const style = css`
    header {
        position: absolute;
        top: 0px;
        background: var(--primary);
        padding: 10px 0px;
        width: 100%;
        max-width: 100vw;
        z-index: 1;
    }

    nav {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        align-items: center;
    }

    span {
        font-weight: bold;
        color: var(--white);
        text-transform: uppercase;
    }

    ul {
        list-style-type: none;
    }

    .trigger {
        display: none;
    }
    .trigger button {
        color: var(--white);
        background: none;
        border: none;
        width: 50px;
        height: 50px;
    }

    .menu {
        display: flex;
    }
    .menu .menu_item {
        position: relative;
        margin: 0px 20px;
    }
    .menu .menu_item::before {
        content: "";
        position: absolute;
        bottom: -2px;
        border-bottom: 1px solid var(--white);
        width: 100%;
        transform-origin: left;
        transform: scaleX(0);
        transition: 0.5s;
    }
    .menu .menu_item:hover::before {
        transform: scaleX(1);
    }

    /* for tablet and smartphone */
    @media screen and (max-width: 768px) {
        .trigger {
            display: block;
        }

        .menu {
            flex: 1 0 100%;
            display: flex;
            flex-direction: column;
            gap: 20px;
            border-top: 1px solid var(--white);
            padding: 10px 0px;
            max-height: 1000px;
            transition: 0.5s;
        }
        .menu.menu--collapse {
            transform: scaleY(0);
            max-height: 0px;
        }

        .menu .menu_item {
            margin: 10px 0px;
            transform: scaleY(1);
            transition: 0.5s;
        }
        .menu.menu--collapse .menu_item {
            transform: scaleY(0);
        }
    }
`;
