import React from "react";
import css from "styled-jsx/css";

import Link from "./Link";
import { LINKEDIN_PROFILE_URI } from "../constants";

const CURRENT_YEAR = new Date().getFullYear();
const RELEASE_YEAR = 2022;

export default function Footer() {
    return (
        <>
            <footer>
                <section className="container">
                    <span>
                        © {RELEASE_YEAR}-{CURRENT_YEAR == RELEASE_YEAR ? "Present" : CURRENT_YEAR},
                        Made with 🚀 by &nbsp;
                        <Link href={LINKEDIN_PROFILE_URI} target="_blank">
                            <b>T-Blaise</b>
                        </Link>
                        . All Rights Reserved.
                    </span>
                </section>
            </footer>

            <style jsx>{style}</style>
        </>
    );
}

const style = css`
    footer {
        background: var(--dark-grey);
        color: var(--light-grey);
        padding: 20px 0px;
        margin-top: 100px;
    }

    b {
        white-space: nowrap;
        color: var(--light-grey);
    }
`;
