/* eslint-disable max-lines-per-function */
import React, { useEffect, useRef, useState } from "react";
import css from "styled-jsx/css";
import { APP_NAME } from "../constants";

import { useNotification } from "../stores/notificationStore/notificationContext";
import FileHelpers from "../utilities/helpers/file.helpers";
import Helpers from "../utilities/helpers/helpers";
import Button from "./Button";

const EXTENTION = "webm";
const MIME_TYPE = `video/${EXTENTION}`;

export default function RecorderWrapper() {
    const { showNotification } = useNotification();

    const mediaRecorderRef = useRef<MediaRecorder>();
    const videoStreamRef = useRef<MediaStream>();
    const recoreURLRef = useRef<string>();

    const [isRecording, setIsRecording] = useState(false);

    const startRecordBtnRef = useRef<HTMLButtonElement>(null);
    const stopRecordBtnRef = useRef<HTMLButtonElement>(null);
    const downloadRecordBtnRef = useRef<HTMLButtonElement>(null);

    const videoWrapperRef = useRef<HTMLDivElement>(null);
    const videoRef = useRef<HTMLVideoElement>(null);

    const isRunned = useRef(false);

    useEffect(() => {
        if (isRunned.current) return;
        isRunned.current = true;

        initRecorder();
    }, []);

    const initRecorder = () => {
        if (
            !(startRecordBtnRef.current && stopRecordBtnRef.current && downloadRecordBtnRef.current)
        ) {
            return;
        }

        startRecordBtnRef.current.addEventListener("click", () => startRecording());
        stopRecordBtnRef.current.addEventListener("click", () => stopRecording());
        downloadRecordBtnRef.current.addEventListener("click", () => downloadRecord());
    };

    const startRecording = async () => {
        if (recoreURLRef.current) return window.location.reload();

        if (isRecording) return;
        setIsRecording(true);

        const audioStream = await navigator.mediaDevices.getUserMedia({
            audio: true
        });
        const audioTrack = audioStream.getAudioTracks()[0];

        videoStreamRef.current = await navigator.mediaDevices.getDisplayMedia({ video: true });
        videoStreamRef.current.addTrack(audioTrack);

        const MediaRecorder = window.MediaRecorder;
        mediaRecorderRef.current = new MediaRecorder(videoStreamRef.current);
        mediaRecorderRef.current.start();

        startVideoStreaming();

        const chunks: Blob[] = [];
        mediaRecorderRef.current.addEventListener("dataavailable", (event) => {
            chunks.push(event.data);
        });

        mediaRecorderRef.current.addEventListener("stop", () => {
            handleStopRecorder(chunks);
        });
    };

    const stopRecording = () => {
        setIsRecording(false);

        mediaRecorderRef.current?.state === "recording" && mediaRecorderRef.current.stop();

        videoStreamRef.current?.getTracks()?.forEach((track) => track.stop());
    };

    const handleStopRecorder = (chunks: Blob[]) => {
        if (!(videoWrapperRef.current && videoRef.current)) return;

        const blob = new Blob(chunks, { type: MIME_TYPE });
        recoreURLRef.current = URL.createObjectURL(blob);

        const video = document.createElement("video");
        video.controls = true;
        video.src = recoreURLRef.current;

        cloneStyle(videoRef.current, video);

        videoWrapperRef.current.lastElementChild?.remove();
        videoWrapperRef.current.append(video);
    };

    const startVideoStreaming = () => {
        if (!videoRef.current) return;

        const video = videoRef.current;
        video.muted = true;

        if ("srcObject" in video) {
            video.srcObject = videoStreamRef.current || null;
        } else {
            (video as any).src = window.URL.createObjectURL(videoStreamRef.current as any);
        }

        video.addEventListener("loadedmetadata", () => {
            video.play();
        });
    };

    const downloadRecord = () => {
        try {
            if (isRecording) return;
            if (!recoreURLRef.current) throw new Error("Record not found.");

            const dateDownload = Helpers.parseDateTime(Date.now());
            const fileName = `Record_${dateDownload}_${APP_NAME}.${EXTENTION}`;
            const onSucceed = () => {
                showNotification({ message: "Download started" });
                return window.location.reload();
            };

            FileHelpers.downloadVideo(recoreURLRef.current, { fileName, onSucceed });
        } catch (error: any) {
            showNotification({ message: error?.message, type: "error" });
        }
    };

    const cloneStyle = (source: HTMLElement, target: HTMLElement) => {
        const styles = window.getComputedStyle(source);

        let cssText = styles.cssText;
        if (!cssText) {
            cssText = Array.from(styles).reduce(
                (prev, property) => `${prev}${property}:${styles.getPropertyValue(property)};`,
                ""
            );
        }

        target.style.cssText = cssText;
    };

    return (
        <>
            <section>
                <div ref={videoWrapperRef} id="preview">
                    <video ref={videoRef} controls></video>
                </div>

                <div className="actions">
                    <Button
                        ref={startRecordBtnRef}
                        id="start-record-btn"
                        block
                        variant="primary"
                        disabled={isRecording}
                    >
                        Start Recording
                    </Button>

                    <Button
                        ref={stopRecordBtnRef}
                        id="stop-record-btn"
                        block
                        variant="primary"
                        disabled={!isRecording}
                    >
                        Stop Recording
                    </Button>

                    <Button
                        ref={downloadRecordBtnRef}
                        id="download-record-btn"
                        block
                        variant="primary"
                        disabled={isRecording}
                    >
                        download Recording
                    </Button>
                </div>
            </section>

            <style jsx>{style}</style>
        </>
    );
}

const style = css`
    section {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        gap: 100px;
    }

    #preview video {
        border: 5px solid var(--primary);
        padding: var(--spacing);
        width: 800px;
        max-width: 100vw;
        overflow: hidden;
    }

    .actions {
        display: flex;
        gap: var(--spacing);
    }

    /* for tablet and smartphone */
    @media screen and (max-width: 768px) {
        .actions {
            flex-direction: column;
        }
    }
`;
