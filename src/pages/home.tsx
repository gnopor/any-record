import React from "react";
import css from "styled-jsx/css";

import HeroSection from "../components/home/HeroSection";
import RecorderWrapper from "../components/RecorderWrapper";
import Page from "../components/Page";

export default function HomePage() {
    return (
        <>
            <Page title="Home">
                <main id="home_page" className="container">
                    <section>
                        <HeroSection />
                    </section>

                    <section id="recorder-wrapper">
                        <RecorderWrapper />
                    </section>
                </main>
            </Page>

            <style jsx>{style}</style>
        </>
    );
}

const style = css`
    main {
        display: flex;
        flex-direction: column;
        gap: 100px;
    }
`;
