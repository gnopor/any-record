/* eslint-disable @next/next/no-sync-scripts */
import React, { useEffect } from "react";
import Head from "next/head";
import { AppProps } from "next/app";

import "../styles/global.css";

import { APP_NAME, WEBAPP_DESCRIPTION, APP_PUBLIC_URI } from "../constants";
import { NotificationProvider } from "../stores/notificationStore/notificationContext";
import ServiceWorkerHelpers from "../utilities/helpers/serviceWorker.helpers";

const WEBAPP_DOMAIN = APP_PUBLIC_URI;

export default function MyApp({ Component, pageProps }: AppProps) {
    useEffect(() => {
        if ("serviceWorker" in navigator) {
            window.addEventListener("load", () =>
                ServiceWorkerHelpers.registerSW("/service-worker.js")
            );
        }
    }, []);

    return (
        <>
            <Head>
                <meta charSet="utf-8" />
                <link rel="icon" href="/favicon.ico" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <meta name="description" content={WEBAPP_DESCRIPTION} />
                <meta name="author" content="Blaise TAYOU" />

                <meta property="og:title" content="PWA-OG is an offline PWA manifest generator." />
                <meta property="og:type" content="website" />
                <meta property="og:url" content={WEBAPP_DOMAIN} />
                <meta property="og:image" content={`${WEBAPP_DOMAIN}/images/logo-xl.png`} />

                <link rel="manifest" href="/manifest.json" />
                <link rel="apple-touch-icon" href="/images/icons/homeScreen96x96.png" />
                <meta name="theme-color" content="#ff6347" />
                <meta name="apple-mobile-web-app-status-ba" content="#ff6347" />

                <title>{APP_NAME}</title>
            </Head>

            <NotificationProvider>
                <Component {...pageProps} />
            </NotificationProvider>
        </>
    );
}
