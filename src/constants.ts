export const APP_NAME = "ANY Record";

export const WEBAPP_DESCRIPTION = `
ANY Record is an offline screen recorder that you can use for free to record your desktop screen, any window on your screen or any tab of your browser. 
You can also download a ready-to-use video (your recording) to share with your team or save on your computer.
    `;

export const APP_REPOSITORY = "https://gitlab.com/gnopor/any-record";

export const LINKEDIN_PROFILE_URI = "https://www.linkedin.com/in/tayou-blaise-9b3a4b191/";

// Environment variables configs
export const API_BASE_URI = process.env.NEXT_PUBLIC_API_BASE_URI || "";

export const APP_PUBLIC_URI = process.env.NEXT_PUBLIC_APP_PUBLIC_URI || "";
