/* eslint-disable no-unused-vars */

export declare global {
    interface Window {
        Prism: any;
    }

    namespace I {
        interface IAlertMessage {
            type?: string;
            title?: string;
            message?: string;
        }
    }
}
